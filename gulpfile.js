var gulp    = require('gulp'),
    sass = require("gulp-sass");
    cssnano = require("gulp-cssnano");
    path    = require('path'),
    concat = require("gulp-concat")
    flatten = require("gulp-flatten")

    srcRoot = "./assets",
    srcFont = srcRoot + "/font",
    srcCss = srcRoot + "/css",
    srcJs = srcRoot + "/js",
    srcImg = srcRoot + "/img",

    webRoot= "/public",
    cssRoot = "public/assets/css",
    fontRoot = "public/assets/font",
    jsRoot = "public/assets/js",
    imgRoot = "public/assets/img";



gulp.task("serve", function serveTask() {
    var browserSync = require("browser-sync");

    var server = browserSync.create();
    var config = {
        server: {
            baseDir: "./",
            index: "index.html",
            routes: {
                "/node_components": "node_components"
            }
        }
    };

    server.init(config);

    gulp
        .watch([
            webRoot + "/**/*.js",
            webRoot + "/**/*.css",
            "*.html",
            webRoot + "/**/*.json",
        ])
        .on("change", server.reload)
    ;
});


gulp.task('build', function() {
	var uglify = require("gulp-uglify");

	gulp
		.src([
            srcJs + "/**/*.js",
            srcJs + "/**/_*.js",
        ])
        .pipe(concat("application.js"))
        .pipe(gulp.dest(jsRoot))
    ;
});

gulp.task('images', function() {
	var extensions = ".{jpg,png,ico,svg,mp4}";
	return gulp
		.src([
			srcImg + "/*" + extensions,
		])
		.pipe(flatten())
		.pipe(gulp.dest(imgRoot));
});


gulp.task('html', function(){
    return gulp
        .src('*.html')
        .pipe(gulp.dest('public/'))
    ;
});


gulp.task('sass', function(){

    gulp
        .src( srcCss + '/src/application.scss')
        .pipe(concat("application.css"))
        .pipe(sass({
            includePaths: [
                "./node_modules/compass-mixins/lib"
            ]
        }).on("error", sass.logError))
        .pipe(cssnano())
        .pipe(gulp.dest(cssRoot))
    ;
});


gulp.task("font", function fontTask() {

	return gulp.src([
		srcFont + "/*.eot",
		srcFont + "/*.svg",
		srcFont + "/*.ttf",
		srcFont + "/*.woff"
	])
		.pipe(flatten())
		.pipe(gulp.dest(fontRoot));
});


gulp.task("watch", function watchTask() {

	gulp.watch([
        srcRoot + "/**/*.js",
        srcRoot + "/**/_*.js"
	], ["build"]);

	gulp.watch([
        srcRoot + "/**/*.scss",
        srcRoot + "/**/_*.scss"
	], ["sass"]);

	gulp.watch([
		"./*.html"
	], ["html"]);

});


gulp.task("prepare", [
	"font",
	"build",
    "images",
	"html",
    "sass"
]);

gulp.task("default", [
	"prepare",
	"watch",
	"serve"
]);
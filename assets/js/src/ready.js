//$(document).ready(function() {
  // la magia aquí
//});

//Pure Javascript
"use strict";

var toggle, tabActive, selectOption, convert;

window.onload = function() {

    var oldActived = "tab-one";
    var newActive;

    //Converting NodeList to List for IE11
    convert = function(nodelist){
        return nodelist = [].slice.call(nodelist);
    }


    tabActive = function(newActive){
        if(newActive == oldActived){
            return;
        }
        convert(document.querySelectorAll("." + oldActived)).forEach(function(node) {
            node.classList.remove("active");
        });
    
        convert(document.querySelectorAll("." + newActive)).forEach(function(node) {
            node.classList.add("active");
        });

        oldActived = newActive;			
    }

    toggle = function(type, element){
        var elementSelector = document.querySelectorAll("." + element);
        var elementChildToggle =  document.querySelectorAll("." + element +" .toggle-element");
        var elementSiblingToggle = document.querySelectorAll("." + type);

        function giveTransition(node){
            node.classList.add("removing-active");
            setTimeout(function(){ 
                node.classList.remove("removing-active");
                node.classList.remove("active");
            },500);
        }

        if(elementChildToggle.length > 0){
            convert(elementChildToggle).forEach(function(node) {
                giveTransition(node);
            });
        }

        if(elementSiblingToggle.length > 0){
            convert(elementSiblingToggle).forEach(function(node) {
                if(!node.classList.contains(element)){
                    giveTransition(node);

                    var classSibling = "";
                    for(var i =0; i< node.classList.length ; i++){
                        classSibling += "."+node.classList[i];
                    }
                    convert(document.querySelectorAll( classSibling +" > .toggle-element")).forEach(function(node) {
                        node.classList.remove("active");
                    });
                }
            });
        }
        
        convert(elementSelector).forEach(function(node) {
            if(node.classList.contains("active")){
                giveTransition(node);
            }else{
                node.classList.add("active");
            }
        });
    }

    selectOption = function(){
        var elementSelected = document.querySelectorAll(".form-options__list__content");
        if(!elementSelected.length){
            return;
        }

        function unselectOption(){
            convert(elementSelected).forEach(function(node) {
                if(node.classList.contains("selected")){
                    node.classList.remove("selected");
                }
            });
        };

        convert(elementSelected).forEach(function(node) {
            node.parentElement.addEventListener("click", function(){
                if(node.classList.contains("selected")){
                    return;
                }else{
                    unselectOption();
                    node.classList.add("selected");
                    document.querySelectorAll(".form-options__list__content.selected input")[0].checked = true;
                }
            });
        });
    }
    selectOption();
}